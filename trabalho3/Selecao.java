/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho3;

import java.util.Random;

/**
 *
 * @author Roberto Almeida
 */
public class Selecao {

    private double[] adaptabilidade;
    private Populacao pais;
    private Populacao atual;

    /**
     *
     * @param p Populacao referente à geração atual.
     */
    public Selecao(Populacao p) {
        this.adaptabilidade = new double[p.getTamanho()];//será por escolha o tamanho da população
        this.pais = new Populacao(p.getTamanho());
        this.atual = p;
        this.setAdaptabilidade(p); //joga os 
        this.selecionaPais();
    }

    private void selecionaPais() {
        for (int i = 0; i < adaptabilidade.length; i++) {
            int pos;
            MersenneTwister mt = new MersenneTwister();
            int num = (int) (0 + (mt.nextDouble() * adaptabilidade[adaptabilidade.length - 1]));
            for (pos = 0; pos < adaptabilidade.length; pos++) {
                if (adaptabilidade[pos] >= num) {
                    pais.setIndividuo(i, atual.getIndividuo(pos));
                    break;
                }
            }
        }
    }

    private void setAdaptabilidade(Populacao p) {
        Individuo individuo = p.getIndividuo(0);
        this.adaptabilidade[0] = individuo.getFitness();
        for (int i = 1; i < this.adaptabilidade.length; i++) {
            individuo = p.getIndividuo(i);
            this.adaptabilidade[i] = individuo.getFitness()
                    + this.adaptabilidade[i - 1];
        }
    }

    public Populacao getPais() {
        return this.pais;
    }
}
