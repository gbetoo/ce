/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho3;

import java.util.BitSet;

/**
 *
 * @author Roberto Almeida
 */
public class Cromossomo {

    private int posReal = 0;// são os contadorres
    private int posInt  = 0;
    private int posBol = 0;
    private int tamanho;
    private int tipo; //0 é binário, 1 é inteiro e 2 é real.

    private BitSet genesBinario = new BitSet();
    private Double[] genesReal;
    private Integer[] genesInt;

    public Cromossomo(int tam, int type) {//tamanho do cromosso (tam array)
        this.tamanho = tam;
        this.tipo = type;
        this.genesReal = new Double[tamanho];
        this.genesInt = new Integer[tamanho];
    }

    public Object getGene(int posicao) {
        if (tipo == 0) {
            return getGeneBinario(posicao);
        } else if (tipo == 1) {
            return getGeneInt(posicao);
        } else {
            return getGeneReal(posicao);
        }
    }

    private Boolean getGeneBinario(int posicao) {
        return (Boolean) (this.getGenesBinario().get(posicao));
    }

    private Integer getGeneInt(int posicao) {
        return this.getGenesInt()[posicao];
    }

    private Double getGeneReal(int posicao) {
        return this.getGenesReal()[posicao];
    }

    public void addGene(Object g) {
        if (this.tipo == 0){ //boolean
        
        }else if (this.tipo == 1){
            addGeneInteger(g);
        }else {
            addGeneReal(g);
        }
    }

    private void addGeneReal(Object g) {
        this.genesReal[posReal] = (Double) g;
        this.posReal++;
    }

    private void addGeneInteger(Object g) {
        this.genesInt[posInt] = (Integer) g;
        this.posInt++;
    }

    /**
     * @return the genesBinario
     */
    public BitSet getGenesBinario() {
        return genesBinario;
    }

    /**
     * @param genesBinario the genesBinario to set
     */
    public void setGenesBinario(BitSet genesBinario) {
        this.genesBinario = genesBinario;
    }

    /**
     * @return the genesReal
     */
    public Double[] getGenesReal() {
        return genesReal;
    }

    /**
     * @param genesReal the genesReal to set
     */
    public void setGenesReal(Double[] genesReal) {
        this.genesReal = genesReal;
    }

    /**
     * @return the genesInt
     */
    public Integer[] getGenesInt() {
        return genesInt;
    }

    /**
     * @param genesInt the genesInt to set
     */
    public void setGenesInt(Integer[] genesInt) {
        this.genesInt = genesInt;
    }
}
