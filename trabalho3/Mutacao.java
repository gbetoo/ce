/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho3;

/**
 *
 * @author Roberto Almeida
 */
public class Mutacao {

    public static void fazMutacao(Populacao p, double taxa) {
        
        for (int i = 0; i < p.getTamanho(); i++) {
            Individuo ind = p.getIndividuo(i);
            for (int j = 0; j < 44; j++) {
                MersenneTwister mt = new MersenneTwister();
                double num = mt.nextDouble();
                if (num < taxa) {
                    ind.swapGene(j);
                }
            }
        }
    }
}
