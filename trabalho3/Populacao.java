/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho3;

/**
 *
 * @author Roberto Almeida
 */
public class Populacao {
    private Individuo[] populacao;
	private int tamanho;
	
	public Populacao() {
		this.populacao = new Individuo[100]; //começa a população com  um array de individuos
		this.tamanho = 100;
	}
	
	public Populacao(int tamanho) {
		this.populacao = new Individuo[tamanho];
		this.tamanho = tamanho;
	}
	
	public Populacao(Individuo[] populacao) {
		this.populacao = populacao;
	}
	
	public int getTamanho() {
		return this.tamanho;
	}
	
	public Individuo getIndividuo(int posicao) {
		return this.populacao[posicao];
	}
	
	public void setIndividuo(int posicao, Individuo i) {
		this.populacao[posicao] = i;
	}
}
