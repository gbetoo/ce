/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho3;

/**
 *
 * @author Roberto Almeida
 */
//
public class Individuo {

    private int[] genes;
    private double fitness;

    public Individuo() {
        this.genes = new int[44];
        this.fitness = 0;
    }

    public Individuo(int[] genes) {
        this.genes = genes;
        this.fitness = 0;
    }

    public int[] getGenes() {
        return this.genes;
    }

    public int getGene(int posicao) {
        return this.genes[posicao];
    }

    public double getFitness() {
        return this.fitness;
    }

    public void setGene(int posicao, int val) {
        this.genes[posicao] = val;
    }

    public void setFitness(double val) {
        this.fitness = val;
    }

    public void setIndividuo(int[] genes) {
        this.genes = genes;
    }

    public void swapGene(int posicao) {
        this.genes[posicao] = 1 - this.genes[posicao];
    }
}
