/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho3;

/**
 *
 * @author Roberto Almeida
 */
public class Main {

    private static int numGeracoes = 100; 
    private static int tamPopulacao = 100;
    private static double taxaCrossover = 0.65;
    private static double taxaMutacao = 0.02;

    public static void main(String[] args) {

        Populacao populacao = iniciaPopulacao();

        int[] genes0 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0};
        
        Individuo ini0 = new Individuo(genes0);
        populacao.setIndividuo(0, ini0);

        int[] genes1 = {0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0,
            0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1,
            1, 0, 1, 1};
        
        Individuo ini1 = new Individuo(genes1);
        populacao.setIndividuo(1, ini1);

        avalia(populacao);

        System.out.println("F6("
                + doubleValue(getX(populacao.getIndividuo(0))) + ", "
                + doubleValue(getY(populacao.getIndividuo(0))) + ") = "
                + populacao.getIndividuo(0).getFitness());

        int[] g0 = populacao.getIndividuo(0).getGenes();
        int[] g1 = populacao.getIndividuo(1).getGenes();

        for (int i = 0; i < g0.length; i++) {
            System.out.print(g0[i] + " ");
        }
        System.out.println();

        for (int i = 0; i < g1.length; i++) {
            System.out.print(g1[i] + " ");
        }
        System.out.println();

        System.out.println("F6("
                + doubleValue(getX(populacao.getIndividuo(1))) + ", "
                + doubleValue(getY(populacao.getIndividuo(1))) + ") = "
                + populacao.getIndividuo(1).getFitness());

        System.out.println("Geração 0");
        imprimeMelhorIndividuo(populacao);

        for (int j = 0; j < tamPopulacao; j++) {
            Individuo ind = populacao.getIndividuo(j);
            System.out.println("Ind [" + (j + 1) + "]::> " + ind.getFitness());
        }

        for (int i = 0; i < numGeracoes; i++) {
            System.out.println("Geração " + (i + 1));

            // seleciona pais
            Selecao s = new Selecao(populacao);
            Populacao pais = s.getPais();

            // faz cruzamento dos pais e gera uma nova populacao
            Populacao novaGer = Crossover.fazCrossover(pais, taxaCrossover);

            // faz a mutação nessa nova população
            Mutacao.fazMutacao(novaGer, taxaMutacao);

            // avalia a nova população
            avalia(novaGer);

            // atualiza essa geracao atual
            populacao = novaGer;

            imprimeMelhorIndividuo(populacao);
            
            //imprime todos os individuos 
//            for (int j = 0; j < tamPopulacao; j++) {
//                Individuo ind = populacao.getIndividuo(j);
//                System.out.println("Ind [" + (j + 1) + "]: " + ind.getFitness());
//            }
//            System.out.println();

        }
    }

    private static void imprimeMelhorIndividuo(Populacao p) {
        Individuo ind = p.getIndividuo(0);
        double melhor = ind.getFitness();
        double media = ind.getFitness();
        int posicao = 0;
        double x = doubleValue(getX(ind));
        double y = doubleValue(getY(ind));
        for (int i = 1; i < tamPopulacao; i++) {
            ind = p.getIndividuo(i);
            media += ind.getFitness();
            if (ind.getFitness() > melhor) {
                melhor = ind.getFitness();
                posicao = i;
                x = doubleValue(getX(ind));
                y = doubleValue(getY(ind));
            }
        }
        media = media / tamPopulacao;

        System.out.println("Média dessa geração: " + media);
        System.out.println("Melhor individuo [" + (posicao + 1) + "]");
        System.out.println("(x: " + x + ", y: " + y + ") = " + melhor + " (melhor))");
        System.out.println();
    }

    private static int binaryToDecimal(int[] vet) {
        int sum = 0;
        for (int i = vet.length - 1; i >= 0; i--) {
            sum += vet[i] * Math.pow(2, vet.length - i - 1);
        }
        return sum;
    }

    private static int[] getX(Individuo ind) {
        int[] binx = new int[22];
        for (int j = 0; j < 22; j++) {
            binx[j] = ind.getGene(j);
        }
        return binx;
    }

    private static int[] getY(Individuo ind) {
        int[] biny = new int[22];
        for (int j = 22; j < 44; j++) {
            biny[j - 22] = ind.getGene(j);
        }
        return biny;
    }

    private static double doubleValue(int[] genes) {
        double dec = binaryToDecimal(genes);
        dec = dec * 200 / (Math.pow(2, 22) - 1);
        dec = dec - 100;
        return dec;
    }

    private static Populacao iniciaPopulacao() {
        Populacao pop = new Populacao(tamPopulacao);
        for (int i = 0; i < tamPopulacao; i++) {
            Individuo ind = new Individuo();
            for (int j = 0; j < 44; j++) {
                double num = Math.random();
                if (num < 0.5) {
                    ind.setGene(j, 1);
                } else {
                    ind.setGene(j, 0);
                }
            }
            pop.setIndividuo(i, ind);
        }
        return pop;
    }

    private static void avalia(Populacao populacao) {
        for (int i = 0; i < tamPopulacao; i++) {
            Individuo ind = populacao.getIndividuo(i);
            double x = doubleValue(getX(ind));
            double y = doubleValue(getY(ind));
            double fitness = 0.5 - (((Math.pow(
                    Math.sin(Math.sqrt(x * x + y * y)), 2)) - 0.5) / (Math.pow(
                    1.0 + 0.001 * (x * x + y * y), 2)));
            ind.setFitness(fitness);
        }
    }
}
