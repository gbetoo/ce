/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho3;

import java.util.Random;

/**
 *
 * @author Roberto Almeida
 */
public class Crossover {

    public static Populacao fazCrossover(Populacao pais, double taxa) {
        Populacao novaGeracao = new Populacao(pais.getTamanho());

        for (int i = 0; i < pais.getTamanho(); i += 2) {
            MersenneTwister mt = new MersenneTwister();
            double num = mt.nextDouble();
            Individuo filho1 = new Individuo();
            Individuo filho2 = new Individuo();

            if (num < taxa) {
                int pos = (int) (0 + (mt.nextDouble() * 43));

                for (int j = 0; j < pos; j++) {
                    filho1.setGene(j, pais.getIndividuo(i).getGene(j));
                    filho2.setGene(j, pais.getIndividuo(i + 1).getGene(j));
                }

                for (int j = pos; j < 44; j++) {
                    filho1.setGene(j, pais.getIndividuo(i + 1).getGene(j));
                    filho2.setGene(j, pais.getIndividuo(i).getGene(j));
                }

                novaGeracao.setIndividuo(i, filho1);
                novaGeracao.setIndividuo(i + 1, filho2);

            } else {
                novaGeracao.setIndividuo(i, pais.getIndividuo(i));
                novaGeracao.setIndividuo(i + 1, pais.getIndividuo(i + 1));
            }
        }

        return novaGeracao;
    }

}
